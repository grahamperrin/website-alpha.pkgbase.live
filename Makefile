.POSIX:
tidy!=(which tidy5 || which tidy)
pandoc!=(which pandoc)

TIDY=$(tidy) -quiet -mi --indent-spaces 4 --wrap 111
PANDOC=$(pandoc) --template template.html --css /style.css -f markdown -t html

.PHONY: help
help: ## Show this help
	@egrep -h '\s##\s' ${.MAKE.MAKEFILES} $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

website: howtos pkg-sets ## update the front-page
	$(PANDOC) < index.md > index.html
	$(TIDY) index.html

howtos: ## update howtos
.for f in index bootstrap jails fresh howdo abi-changes poudriere-jails
	$(PANDOC) < howto/${f}.md > howto/${f}.html
	$(TIDY) howto/${f}.html
.endfor

pkg-sets: # update the package sets
.for f in current stable release
	$(PANDOC) < ${f}/index.md > ${f}/index.html
	$(TIDY) ${f}/index.html
.endfor

update-static: website ## update the entire website
	mkdir -p build/howto build/current build/stable build/release
	cp alpha.pkgbase.live.pub build/
	cp favicon.png build/
	cp style.css build/
	mv index.html build/
	mv howto/*.html build/howto/
	mv current/index.html build/current/
	mv stable/index.html build/stable/
	mv release/index.html build/release/
	mkdir -p site/
	cp -r build/* site/
	rm -r build
